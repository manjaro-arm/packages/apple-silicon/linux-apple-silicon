# AArch64 Apple Silicon
# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Janne Grunau <j@jannau.net>

buildarch=8

_rcver=6.3
#_rcrel=3
_asahirel=13
pkgrel=3

_m1n1_version=1.2.8

pkgbase=linux-apple-silicon
#_commit_id=asahi-${_nextver}-${_asahirel}
_commit_id=asahi-${_rcver}${_rcrel+-rc}${_rcrel}-${_asahirel}
_srcname=linux-${_commit_id}
_kernelname=${pkgbase#linux}
_desc="AArch64 Apple Silicon (M1 development kernel)"
pkgver="${_rcver}${_rcrel:+rc}${_rcrel}"
arch=('aarch64')
url="http://www.kernel.org/"
license=('GPL2')

makedepends=(
  bc dtc kmod libelf pahole cpio perl rust rust-src rust-bindgen tar xz xmlto
)
options=('!strip')
source=(
  https://github.com/AsahiLinux/linux/archive/${_commit_id}.tar.gz
  config         # the main kernel config file
  config.edge    # overrides for linux-apple-silicon-edge
)
sha256sums=('d4665058f7713f3220ff5eb684ef3bb5febc75f72bf6f399389131cfc0730f21'
            'fc59ba16599a7c2692dc5d675a519e128a93aec2e8e7d6385e3de6dc2fd709a4'
            '1f7021809b7ac8fa9e8b12a6ec14165464a55b26d1835fa6cb8c63fe57132f2b')
b2sums=('c9d38f1bf522fb4330f060581d0e6a338c155e37cfb7ba4215fcab170077add51b7fa69676167bc3284cd109cc43ac3121dc6b8a06d34ad9c48017c5b53f4e14'
        'e9c699db38c8ff9645cf11574ea894d6afdd95f3782047769ab9ec79b2e9145f848789c87ffc2e58a0e396f6bcebff33f4d3f2f0a3efb6380e4563e4b488448d'
        '33a023a0fe6e6316fbe32a1bb503fcc9afbf0f30c851329521faab9a54b5f3ff914606ebcd9e590e3b7e547f3841f215838a09eaa4fe0f52929334f9805960b3')
export KBUILD_BUILD_HOST=archlinux
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
  cd $_srcname
  
  rm localversion.05-asahi
  
  echo "Setting version..."
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux}" > localversion.20-pkgname

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
  
  # Fix for newer bindgen versions
  sed -i -e s/blacklist/blocklist/ \
    -e s/whitelist/allowlist/ \
    -e s/--size_t-is-usize// \
    rust/Makefile

  echo "Creating build directories..."
  mkdir -p build/base build/edge

  echo "Setting config (base)..."
  cp ../config build/base/.config
  make olddefconfig prepare O=$PWD/build/base
  diff -u ../config build/base/.config || :
  make -s kernelrelease O=$PWD/build/base > build/base/version

  echo "Setting config (edge)..."
  cat build/base/.config ../config.edge > build/edge/.config
  make olddefconfig prepare O=$PWD/build/edge
  make -s kernelrelease O=$PWD/build/edge > build/edge/version

  echo "Prepared $pkgbase version $(<build/base/version)"
}

build() {
  cd $_srcname
  echo "Building base..."
  make all O=$PWD/build/base
  echo "Copying objects..."
  cp build/edge/.config{,.save}
  touch build/base/.config
  cp -urT build/{base,edge}
  rm build/edge/init/version.o
  make O=$PWD/build/edge oldconfig prepare
  cp build/edge/.config{.save,}
  echo "Building edge..."
  make O=$PWD/build/edge oldconfig prepare
  make O=$PWD/build/edge all
}

_package_kernel() {
  pkgdesc="The $pkgdesc kernel and modules"
  depends=(coreutils kmod initramfs $2 "m1n1>=$_m1n1_version")
  optdepends=('crda: to set the correct wireless channels of your country'
              'linux-firmware: firmware images needed for some devices')
  provides=(WIREGUARD-MODULE linux=${pkgver})
  replaces=(wireguard-arch)

  local kernver="$(<$O/version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$O"/arch/arm64/boot/Image "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$1" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  make O="$O" INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 modules_install

  echo "Installing device trees..."
  install -Dt "$modulesdir/dtbs" "$O"/arch/arm64/boot/dts/apple/*.dtb

  # remove build and source links
  rm "$modulesdir"/{source,build}
}

_package() {
  cd $_srcname
  export O="$PWD/build/base"
  _package_kernel "$pkgbase"
}

_package-edge() {
  cd $_srcname
  export O="$PWD/build/edge"
  _package_kernel "$pkgbase-edge" "$pkgbase=$pkgver"
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)

  KARCH=arm64

  cd $_srcname
  O=build/base
  local builddir="$pkgdir/usr/lib/modules/$(<${O}/version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 $O/.config Makefile $O/Module.symvers $O/System.map \
    localversion.* $O/version $O/vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  cp -t "$builddir" -a scripts $O/scripts

  # required when STACK_VALIDATION is enabled
  #install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # required when DEBUG_INFO_BTF_MODULES is enabled
  #install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids

  echo "Installing headers..."
  cp -t "$builddir" -a include $O/include

  install -Dt "${builddir}/arch/${KARCH}" -m644 arch/${KARCH}/Makefile
  install -Dt "${builddir}/arch/${KARCH}/kernel" -m644 $O/arch/${KARCH}/kernel/asm-offsets.s
  cp -t "${builddir}/arch/${KARCH}" -a arch/${KARCH}/include $O/arch/${KARCH}/include

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */${KARCH}/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Removing cmd files..."
  find "$builddir" -type f -name '*.cmd' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"

  O=build/edge
  local builddir2="$pkgdir/usr/lib/modules/$(<${O}/version)/build"
  mkdir -p $builddir2
  cp -rl $builddir/* $builddir2
  cp --remove-destination $O/{.config,System.map,version,Module.symvers,vmlinux} $builddir2
  rm -rf $builddir2/include/config
  cp -r $O/include/config $builddir2/include/config
}

pkgname=("$pkgbase" "$pkgbase-headers" "$pkgbase-edge")
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
